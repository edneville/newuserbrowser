#!/bin/sh

preferences() {
    if test -f "$CONFIG/prefs.js"; then
        cat "$CONFIG/prefs.js"
    fi
}

newuser() {
    while /bin/true; do
        DIR=`mktemp -d`
        SUFFIX=`echo "$DIR" | sed -e 's/.*\///g'`

        id "${USR}_${SUFFIX}" >/dev/null 2>/dev/null
        if test $? -eq 0; then
            rmdir "${DIR}"
            continue
        fi

        echo "${DIR}"
        break
    done
}


BROWSER="${BROWSER:-firefox}"
#BROWSER="/usr/local/firefox/firefox"
USR="${SUDO_USER:-$USER}"
HOMEDIR=`getent passwd "$USR" | cut -d: -f6`
PAGE="${1:-file://$HOMEDIR/homepage.html}"
CONFIG="$HOMEDIR/.config/newuserbrowser"
EXTENSIONS="$CONFIG/extensions"
PREFS=`preferences`
DIR=`newuser ${USR}`
SUFFIX=`echo "$DIR" | sed -e 's/.*\///g'`
OPENVPN="${OPENVPN:-true}"
OPENVPNHOME="/usr/local/data/openvpn"
OPENVPNCRT="$OPENVPNHOME/ca.rsa.2048.crt"
OPENVPNCRL="$OPENVPNHOME/crl.rsa.2048.pem"
OPENVPNCFG="$OPENVPNHOME/openvpn.ovpn"
OPENVPNAUTH="$OPENVPNHOME/openvpn_auth.txt"
OPENVPNTUN="$OPENVPNHOME/openvpntun.sh"
NEWUSER="${USR}_${SUFFIX}"

pulse_audio_init() {
    if test ! -d "${HOMEDIR}/.pulse"; then
        mkdir -p "${HOMEDIR}/.pulse.$$" "${HOMEDIR}/.pulse"
        chown "${USR}:${USR}" "${HOMEDIR}/.pulse.$$" "${HOMEDIR}/.pulse"
        cp /etc/pulse/default.pa "${HOMEDIR}/.pulse.$$/"
        chmod 644 "${HOMEDIR}/.pulse.$$/default.pa"
        chown "${USR}:${USR}" "${HOMEDIR}/.pulse.$$/default.pa"
        mv "${HOMEDIR}/.pulse.$$/default.pa" "${HOMEDIR}/.pulse/"
        rm -rf "${HOMEDIR}/.pulse.$$"
    fi

    grep 'load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1' "${HOMEDIR}/.pulse/default.pa" >/dev/null 2>/dev/null
    if test $? -ne 0; then
        echo 'load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1' >> "${HOMEDIR}/.pulse/default.pa"
    fi

    grep "load-module module-native-protocol-unix auth-anonymous=1 socket=/tmp/${USR}-pulse-socket" "${HOMEDIR}/.pulse/default.pa" >/dev/null 2>/dev/null
    if test $? -ne 0; then
        echo "load-module module-native-protocol-unix auth-anonymous=1 socket=/tmp/${USR}-pulse-socket" >> "${HOMEDIR}/.pulse/default.pa"
    fi

    # if USR does not have a pulseaudio daemon then start one
    # often the WM will start pulse audio and it wont use a unix socket
    pgrep -u "${USR}" -f 'pulseaudio.*(--start|-D|--daemonize)' >/dev/null 2>/dev/null
    if test $? -eq 0; then
        pkill -u "${USR}" -f 'pulseaudio.*(--start|-D|--daemonize)'
        if test -f "/tmp/${USR}-pulse-socket"; then
            rm "/tmp/${USR}-pulse-socket"
        fi

        echo "pulseaudio -k; pkill -u \"${USR}\" -f \"pulseaudio\"; pulseaudio -D --log-target=syslog --disallow-module-loading=false --system; pacmd \"set-sink-mute 0 false\"; pacmd \"set-sink-volume 0 20000\"" | su - ${USR} 
    fi
}

font_config() {
    if test -d "${HOMEDIR}/.config/fontconfig"; then
        mkdir -p "${DIR}/.config"
        cp -R "${HOMEDIR}/.config/fontconfig" "${DIR}/.config/"
    fi
}

openvpn_script() {
    ns="$1"

    cat << EOF
#!/bin/sh
case \$script_type in
        up)
                ip netns add $ns
                ip netns exec $ns ip link set dev lo up
                ip link set dev "\$1" up netns $ns mtu "\$2"
                ip netns exec $ns ip addr add dev "\$1" \\
                        "\$4/\${ifconfig_netmask:-30}" \\
                        \${ifconfig_broadcast:+broadcast "\$ifconfig_broadcast"}
                if [ -n "\$ifconfig_ipv6_local" ]; then
                        ip netns exec $ns ip addr add dev "\$1" \\
                                "\$ifconfig_ipv6_local"/112
                fi
                ;;
        route-up)
                ip netns exec $ns ip route add default via "\$route_vpn_gateway"
                if [ -n "\$ifconfig_ipv6_remote" ]; then
                        ip netns exec $ns ip route add default via \\
                                "\$ifconfig_ipv6_remote"
                fi
                ;;
        down)
                ip netns delete $ns
                ;;
esac
EOF
}

create_user() {
    echo "Dir: $DIR"
    useradd -d "$DIR" -s /bin/bash "${NEWUSER}"

    if test ! -d "$OPENVPNHOME"; then
        OPENVPN=false
    fi

    if test "${OPENVPN}x" = "truex"; then
        cp "$OPENVPNCRT" "$OPENVPNCRL" "$OPENVPNCFG" "$OPENVPNAUTH" "${DIR}"
        chmod 755 "${DIR}/netns_script"
    fi

    grep pulse-access /etc/group >/dev/null 2>/dev/null
    if test $? -eq 0; then
        gpasswd -a "${NEWUSER}" pulse-access
        gpasswd -a "${NEWUSER}" pulse
        mkdir -p "${DIR}/.pulse" "${DIR}/.config/nixpkgs" "${DIR}/.config/pulse"
        # echo "default-server = 127.0.0.1" | tee -a "${DIR}/.pulse/client.conf" "${DIR}/.config/pulse/client.conf"
        echo "default-server = unix:/tmp/${USR}-pulse-socket" | tee -a "${DIR}/.pulse/client.conf"
        # echo "autospawn = yes" | tee -a "${DIR}/.pulse/client.conf" "${DIR}/.config/pulse/client.conf"
        echo "{ pulseaudio = true; }" >> "${DIR}/.config/nixpkgs/config.nix"
        chown -R "${NEWUSER}:${NEWUSER}" "${DIR}"
        echo "pulseaudio -k; pulseaudio --log-target=syslog -D" | su - "${NEWUSER}"
    fi

    if test -d "$EXTENSIONS"; then
        mkdir -p "${DIR}/.mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/"
        cp -Rp "${EXTENSIONS}/." "${DIR}/.mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/"
    fi

    PROFILE=`echo "${NEWUSER}" | sed -e 's/.*\.//g' -e 's/^..//g' | tr [:upper:] [:lower:]`
    INI="[General]
    StartWithLastProfile=1

    [Profile0]
    Name=default-release
    IsRelative=1
    Path=${PROFILE}.default
    
    [InstallCBDE0CC28E6567B7]
    Default=${PROFILE}.default
    Locked=1
    "

    mkdir -p "${DIR}/.mozilla/firefox/${PROFILE}.default/chrome"

    echo "${INI}" > "${DIR}/.mozilla/firefox/profiles.ini"
    echo "@-moz-document domain(reddit.com){ html, p, li, h1 {font-family : sans-serif !important;} }
    @font-face { font-family: 'helvetica neue'; src: local('Arial'); }
    @font-face { font-family: 'helveticaneue'; src: local('Arial'); }" > "${DIR}/.mozilla/firefox/${PROFILE}.default/chrome/userContent.css"

    echo "$PREFS" >> "${DIR}/.mozilla/firefox/${PROFILE}.default/prefs.js"

    chown -R "${NEWUSER}:${NEWUSER}" "$DIR"
}

start_browser() {
    PROFILE=`echo "${NEWUSER}" | sed -e 's/.*\.//g' -e 's/^..//g' | tr [:upper:] [:lower:]`

    if test "${OPENVPN}x" = "truex"; then
        ( cd "$DIR" && /usr/sbin/openvpn --config $OPENVPNCFG --ifconfig-noexec --route-noexec --up ${DIR}/netns_script --route-up ${DIR}/netns_script --down ${DIR}/netns_script --script-security 2 --writepid "$DIR/openvpn.pid" & )
        mkdir -p "/etc/netns/${SUFFIX}"
        echo "nameserver 1.1.1.1" > "/etc/netns/${SUFFIX}/resolv.conf"

        while /bin/true; do
            ip netns list | grep "$SUFFIX" >/dev/null && break
            sleep 1
        done

        echo "$BROWSER --profile ${DIR}/.mozilla/firefox/${PROFILE} '$PAGE'"
        ip netns exec ${SUFFIX} su - "${NEWUSER}" bash -c "$BROWSER --profile ${DIR}/.mozilla/firefox/${PROFILE}.default '$PAGE'"
    else
        su - "${NEWUSER}" bash -c "$BROWSER -P $PROFILE '$PAGE'"
    fi
}

openvpn_shutdown() {
    if test -f "$DIR/openvpn.pid"; then
        pkill -9 -f "/usr/sbin/openvpn.*${SUFFIX}"
    fi
}

cleanup() {
    pkill -9 -u "${NEWUSER}"

    while /bin/true; do
        pgrep -u "${NEWUSER}" >/dev/null
        if test $? -eq 0; then
            pkill -9 -u "${NEWUSER}"
            sleep 1
            continue
        fi
        break
    done
    userdel -f -r "${NEWUSER}" >/dev/null 2>/dev/null
    if test -d "/etc/netns/${SUFFIX}"; then
        rm -rf "/etc/netns/${SUFFIX}"
    fi

    if test "${OPENVPN}x" = "truex"; then
        ip netns list | grep "$SUFFIX" >/dev/null
        if test $? -eq 0; then
            ip netns delete "$SUFFIX"
        fi
    fi
}

pulse_audio_init
font_config

xhost "local:${NEWUSER}" >/dev/null

if test "${OPENVPN}x" = "truex"; then
    ( openvpn_script "${SUFFIX}" ) > "${DIR}/netns_script"
fi

create_user
start_browser
openvpn_shutdown
cleanup

