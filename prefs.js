// user_pref("network.proxy.ftp", "YOUR_PROXY_SERVER"); 
// user_pref("network.proxy.ftp_port", YOUR_PROXY_PORT); 
// user_pref("network.proxy.http", "YOUR_PROXY_SERVER"); 
// user_pref("network.proxy.http_port", YOUR_PROXY_PORT); 
// user_pref("network.proxy.share_proxy_settings", true); 
// user_pref("network.proxy.socks", "YOUR_PROXY_SERVER"); 
// user_pref("network.proxy.socks_port", YOUR_PROXY_PORT); 
// user_pref("network.proxy.ssl", "YOUR_PROXY_SERVER"); 
// user_pref("network.proxy.ssl_port", YOUR_PROXY_PORT); 
// user_pref("network.proxy.type", 1); 

// user_pref("network.proxy.http", "bf.s5h.net"); 
// user_pref("network.proxy.http_port", 8888 ); 
// user_pref("network.proxy.type", 1); 

user_pref("middlemouse.contentLoadURL", true);

user_pref("security.sandbox.content.level", 0);
user_pref("security.sandbox.content.syscall_whitelist", 16);
user_pref("security.sandbox.content.write_path_whitelist", "/dev/snd/" );
user_pref("dom.event.clipboardevents.enabled", false );

user_pref("extensions.shownSelectionUI", true);
user_pref("extensions.autoDisableScopes", 0);

user_pref("dom.webnotifications.enabled", false);
user_pref("dom.webnotifications.enabled", false);
user_pref("dom.webnotifications.serviceworker.enabled", false);
user_pref("dom.pushconnection.enabled", false);
user_pref("dom.push.enabled", false);

// user_pref("services.sync.prefs.sync.dom.webnotifications.enabled", true);
// user_pref("services.sync.prefs.sync.dom.webnotifications.serviceworker.enabled", true);
// user_pref("services.sync.prefs.sync.dom.pushconnection.enabled", true);
// user_pref("services.sync.prefs.sync.dom.push.enabled", true);
