
# about

This simple script crates a user and spawns a browser for that user. When you close the browser the user is purged. The intention is to provide some isolation between your desktop user and a more private burnable browser instance.

# what should i know

This will bounce your pulse audio daemon if you don't permit connections 127.0.0.1. This is to allow another user to connect to the daemon.

New account directories will reside in `/tmp`.

If the system boots during use, user accounts will not be purged on startup.

# why

Creating a user and spawning a browser ensures there is no user session crossover from a desktop user into the newly spawned browser.

There are some things that cannot easily be addressed:

 * fonts
 * outbound IP address
 * colour depth
 * screen resolution
 * etc

# how to use

Copy the script to your home directory or somewhere for all users (/usr/local/bin, for example) and `chmod +x`.

Run it as:

  `sudo userbrowser.sh https://www.google.co.uk`

or change the `PAGE` value within the script to set a default location. Personally, I like a bookmarks page.

# customisation

If you have extensions that you would like to see in your new browser environment, place them in the location `$HOME/.config/newuserbrowser/extensions`, they will be copied when the browser is started.

# todo

 * fonts
 * randomise size
 * do-not-track
 * customisation for chrome

